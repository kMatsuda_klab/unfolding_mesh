#ifndef face_h
#define face_h

#include "vec.h"
#include "Vertex.h"
#include "Edge.h"

class Face{
private:
	Vertex *v_address[3];
	Edge *e_address[3];

public:
	//the id of vertex
	int v_id[3];
	double natural_area;

public:
	//constructor
	Face(int _i, int _j, int _k);
	void setVertex(Vertex *V0, Vertex *V1, Vertex *V2);
	void setEdge(Edge *E0, Edge *E1, Edge *E2);
	//calculate the force to Vertex_id
	void forceFace();
	void forceNormal(double _phys_time);
	//void forceVolume(double _volume, double _natural_volume);
	void forceAngle();
	_vec<double> calcNormal();
};


#endif