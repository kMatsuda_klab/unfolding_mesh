#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <string>

#include "parameter.h"
#include "FixPolygon.h"

using namespace std;

FixPolygon::FixPolygon(){
	string buf;

	fstream ifs;
	ifs.open("./mesh_data/"+fix_name+".obj", ios::in);
	if(!ifs.is_open()){	cout << "cannot open file!" << endl;	exit(1);}

	while(getline(ifs, buf)){
		stringstream ss;
		string v_f_flag;
		ss << buf;
		ss >> v_f_flag;

		if(v_f_flag == "v"){
			double data[3] = {0};
			for(int i = 0; i < 3; ++i){	ss.ignore();	ss >> data[i];}
			Vertex tmp(data[0], data[1], data[2]);							FixVertices.push_back(tmp);
		}
	}
}