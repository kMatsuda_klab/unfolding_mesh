#ifndef vertex_h
#define vertex_h

#include <vector>
#include "vec.h"
#include "parameter.h"

using namespace std;

class Vertex{
private:
	_vec<double> frc;
	_vec<double> frc_omp[THREAD_NUM];
	double g;
	int color;
	int node_id;

public:
	_vec<double> loc;
	int fix;
	vector<int> ConnectedVertices;
	_vec<double> vertex_normal;

public:
	Vertex(double _x, double _y, double _z);
	void Force(_vec<double> _f);
	void Force_omp(_vec<double> f);
	void ForceToZero();
	void ForceToZero_omp();
	void reduceForce_omp();

	_vec<double> call_frc();
	
	int vi;

	void push_color(int);
	int pull_color(void);

	void push_nodeid(int);
	int pull_nodeid(void);
};


#endif