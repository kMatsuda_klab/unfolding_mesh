#ifndef fixpolygon_h
#define fixpolygon_h

#include <vector>
#include "Vertex.h"

class FixPolygon{
public:
	std::vector<Vertex> FixVertices;
	std::vector<int> FixVertexID;
	FixPolygon();
};

#endif
