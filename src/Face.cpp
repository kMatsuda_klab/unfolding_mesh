#include <math.h>

#include "vec.h"
#include "parameter.h"
#include "Vertex.h"
#include "Edge.h"
#include "Face.h"

//constructor and Set the Vertex and Edge
Face::Face(int _i,int _j, int _k){
	v_id[0] = _i;
	v_id[1] = _j;
	v_id[2] = _k;
}

void Face::setVertex(Vertex *V0, Vertex *V1, Vertex *V2){
	v_address[0] = V0;
	v_address[1] = V1;
	v_address[2] = V2;
}

void Face::setEdge(Edge *E0, Edge *E1, Edge *E2){
	e_address[0] = E0;
	e_address[1] = E1;
	e_address[2] = E2;
}


//Calculate the force
void Face::forceFace(){
	for(int i = 0; i < 3; ++i){
		_vec<double> V0, V1, V2;

		V0 = v_address[i%3]->loc;
		V1 = v_address[(i+1)%3]->loc;
		V2 = v_address[(i+2)%3]->loc;

		double area = calcNormal().norm() / 2.0;

		if(area < 1e-8)	area += 1e-8;

		_vec<double> force_vector = (V0 % V1 + V1 % V2 + V2 % V0) % (V2 - V1);
		double frc = -spring_k_f*(area - natural_area) / (4*area);

		//v_address[i] -> Force(frc*force_vector);
		v_address[i]->Force_omp(frc*force_vector);
	}
}

void Face::forceNormal(double _phys_time){
	_vec<double> force_vector = calcNormal();
	force_vector *= 0.1*k_n + 0.9*k_n*pow(_phys_time, 3.0) / (pow(1.0, 3.0) + pow(_phys_time, 3.0));

//	force_vector *= k_n * (1 + exp(-_phys_time));
	//force_vector *= k_n;
	for(int i = 0; i < 3; ++i){
		//v_address[i] -> Force(force_vector);
		v_address[i]->Force_omp(force_vector);
	}
}

void Face::forceAngle(){
	for(int i = 0; i < 3; ++i){
		_vec<double> force = _vec<double> (0.0, 0.0, 0.0);

		for(int j = 0; j < 3; ++j){
			_vec<double> Evi = e_address[i]->edgeVector();
			_vec<double> Eei = e_address[j]->edgeVector();
			_vec<double> Normal = calcNormal();

			double DeltaTheta = e_address[j]->angleDisplacement();
			double coefficient1 = spring_k_a*DeltaTheta*(Evi*Eei);
			double coefficient2 = Normal*Normal;
			coefficient2 *= Eei.norm();
			if(coefficient2 < 1e-8)	coefficient2 += 1e-8;
			coefficient1 /= coefficient2;
			force += coefficient1 * Normal;
			
		}

		//v_address[i] -> Force(force);
		v_address[i]->Force_omp(force);
	}
}

//Calculate the feature value
_vec<double> Face::calcNormal(){
	_vec<double> edge0 = v_address[2] -> loc - v_address[1] -> loc;
	_vec<double> edge1 = v_address[0] -> loc - v_address[2] -> loc;
	return edge0 % edge1;
}
