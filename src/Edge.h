#ifndef edge_h
#define edge_h

#include "vec.h"
#include "Vertex.h"

class Edge{
private:
	double theta;
	double natural_angle;
	double natural_length;
	//Pointer of Vertex
	Vertex *start_address, *end_address;

public:
	//Vertex, Face, and Counterpart Edge
	int start, end;
	int belongFace_id;
	int counterpart_id;

public:
	//constructor, and set the natural angle and length
	Edge(int Face_id, int _i,int _j);
	void setVertex(Vertex *Start, Vertex *End);
	void setNaturalAngle();
	void setNaturalLength(double _l);
	//Calculate the force
	void forceEdge();
	//Calculate the angle, vector of edge, angle displacement
	void calcAngle(_vec<double> v0, _vec<double> v1);
	_vec<double> edgeVector();
	double angleDisplacement();
};


#endif