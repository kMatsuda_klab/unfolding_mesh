#include "Vertex.h"
#include <omp.h>

Vertex::Vertex(double _x, double _y, double _z){
	loc = _vec<double> (_x, _y, _z);
	frc = _vec<double> (0.0, 0.0, 0.0);
	vertex_normal = _vec<double>(0.0, 0.0, 0.0);
	g = 5.0;
}

_vec<double> Vertex::call_frc(){
	return frc;
}

void Vertex::Force_omp(_vec<double> _f) {
	frc_omp[omp_get_thread_num()] += _f;
}

void Vertex::Force(_vec<double> _f){
	frc += _f;
}

void Vertex::ForceToZero(){
	frc.IN(0.0,0.0,0.0);
}

void Vertex::ForceToZero_omp() {
	for( int i = 0; i < THREAD_NUM; i++)
		frc_omp[i].IN(0., 0., 0.);
}

void Vertex::reduceForce_omp() {
	ForceToZero();
	for (int i = 0; i < THREAD_NUM; i++) {
		frc += frc_omp[i];
	}
	ForceToZero_omp();
}

void Vertex::push_color(int c){
	color = c;
}

int Vertex::pull_color(){
	return color;
}

void Vertex::push_nodeid(int i){
	node_id = i;
}

int Vertex::pull_nodeid(){
	return node_id;
}