#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>

#include <algorithm>
#include <iterator>

#include <sys/stat.h>						/*to make an output folder*/
#include "vec.h"						/*vector libarary made by prof.Inoue*/

#include "parameter.h"
#include "Vertex.h"
#include "Edge.h"
#include "Face.h"
#include "FixPolygon.h"

#include <cvode/cvode.h>               /* prototypes for CVODE fcts., consts.  */
//#include <nvector/nvector_serial.h>    /* serial N_Vector types, fcts., macros */
#include <nvector/nvector_openmp.h>    /* OpenMP N_Vector types, fcts., macros */
#include <sunlinsol/sunlinsol_spgmr.h> /* access to SPGMR SUNLinearSolver      */
#include <cvode/cvode_spils.h>         /* access to CVSpils interface          */
#include <cvode/cvode_direct.h>        /* access to CVDls interface            */
#include <sundials/sundials_types.h>   /* definition of type realtype          */
#include <sundials/sundials_math.h>    /* definition of ABS and EXP            */

#include <omp.h>

#include <time.h>

using namespace std;

//the variable array of Vertex, Edge, and Face
vector<Edge> edges;
vector<Vertex> vertices;
vector<Face> faces;
int vertices_size;
int edges_size;
int faces_size;
FixPolygon fixpolygon;


#ifdef COLLISION
// variables for collision
double collision_distance;

class _list{
public:
//	vector<Vertex*> pp;
	vector<int> i;
};

vector<_list> node;
double _list_dsize;
int SYS_SIZE = 1;
_vec<double> SHIFT;


void db_screen( _vec<double> a ){
	std::cout << a.x << " " << a.y << " " << a.z << std::endl;
}

void Initial_Registration_list(){
	for(auto np = node.begin(); np != node.end(); ++np){
		if(!np->i.empty())	np->i.clear();
	}

	int chk = 0;
	for(int i = 0; i < vertices.size(); ++i){
		Vertex* v = &vertices[i];
		_vec<double> loc = v->loc + SHIFT;
		loc /= _list_dsize;
		_vec<int> iloc = loc.icast();
		if(iloc.x < 0 || iloc.y < 0 || iloc.z < 0 || iloc.x >= SYS_SIZE || iloc.y >= SYS_SIZE || iloc.z >= SYS_SIZE){
			chk = 1;
			cout << SYS_SIZE << endl;
			cout << "Error - initial registration list" << endl;
			exit(1);
		}else{
			int index = iloc.x*SYS_SIZE*SYS_SIZE + iloc.y*SYS_SIZE + iloc.z;
			node[index].i.push_back(v->vi);
			v->push_nodeid(index);
		}
	}
}




void Init_Collision_list(){
	_list_dsize = collision_distance;
//	_list_dsize = collision_distance * 1.20;

	Vertex* tmp_v = &vertices[0];
	_vec<double> min = tmp_v->loc;
	_vec<double> max = tmp_v->loc;

//	_vec<double> min(1000., 1000., 1000.);
//	_vec<double> max(-1000., -1000., -1000.);

	for(auto v: vertices){
		if(v.loc.x < min.x)	min.x = v.loc.x;
		if(v.loc.x > max.x) max.x = v.loc.x;

		if(v.loc.y < min.y)	min.y = v.loc.y;
		if(v.loc.y > max.y)	max.y = v.loc.y;

		if(v.loc.z < min.z)	min.z = v.loc.z;
		if(v.loc.z > max.z)	max.z = v.loc.z;
	}


	const double tmp_margin_n = 2.0;
	_vec<double> box_size = max - min;	box_size *= tmp_margin_n;
	
	SHIFT = -1.0*min + _list_dsize*0.50 + box_size*(tmp_margin_n - 1.)*0.50/ tmp_margin_n;
	
	
//	std::cout << SHIFT.x << " " <<  SHIFT.y << " " <<  SHIFT.z << std::endl; exit(0);
	
//	_vec<double> box_size = max - min;	box_size *= 5.0;
//	SHIFT = -1.0*min + _list_dsize*0.50 + box_size*0.40;

	box_size /= _list_dsize;
	_vec<int> tmp_sys_size = box_size.icast();

	SYS_SIZE = tmp_sys_size.x;
	if(SYS_SIZE < tmp_sys_size.y) SYS_SIZE = tmp_sys_size.y;
	if(SYS_SIZE < tmp_sys_size.z) SYS_SIZE = tmp_sys_size.z;

	node.reserve(10000);
	_list ptcl_tmp;
	node.resize(SYS_SIZE * SYS_SIZE * SYS_SIZE, ptcl_tmp);
//	node.resize(SYS_SIZE * SYS_SIZE * SYS_SIZE);

	for(int i = 0; i < vertices.size(); ++i){
		Vertex *pi = &vertices[i];
		pi->vi = i;
	}

	(void)Initial_Registration_list();

//	for(int i = 0; i < vertices.size(); ++i){
//		Vertex *pi = &vertices[i];
//		for(auto vi: pi->ConnectedVertices){
//			Vertex *pk = &vertices[vi];
//			if(pi == pk) pi->vi = vi;
//		}
//	}
}
#endif


void OBJloader(){
	string buf;

	fstream ifs;
	ifs.open("./mesh_data/"+org_name+".obj", ios::in);
	if(!ifs.is_open()){	cout << "cannot open input file!" << endl;	exit(1);}

	// read to the end of file
	while(getline(ifs, buf)){
		stringstream ss;
		string v_f_flag;
		ss << buf;
		ss >> v_f_flag;

		if(v_f_flag == "v"){
			double data[3] = {0};
			for(int i = 0; i < 3; ++i){
				ss.ignore();
				ss >> data[i];
			}
			Vertex tmp(data[0], data[1], data[2]);								vertices.push_back(tmp);
		}else if(v_f_flag == "f"){
			int data[3] = {0};
			for(int i = 0; i < 3; ++i){
				ss.ignore();
				ss >> data[i];
			}
			Face tmp(data[0]-1, data[1]-1, data[2]-1);							faces.push_back(tmp);
		}
	}
	ifs.close();

	// make an output folder
	string output_folder_name = "output_" + org_name;
	mkdir(output_folder_name.c_str(),0777);
}


void set_color(){
	for(int i = 0; i < vertices.size(); ++i){
		Vertex *pi = &vertices[i];
		if(pi->loc.x < 0.0){
			pi->push_color(-1);
		}else{
			pi->push_color(1);
		}
	}
}


//Set polygon
void init(){
	OBJloader();

	vertices_size = vertices.size();
	faces_size = faces.size();

	// set edge
	for(int i = 0; i < faces_size; ++i){
		Edge edge0(i, faces[i].v_id[1], faces[i].v_id[2]);						edges.push_back(edge0);
		Edge edge1(i, faces[i].v_id[2], faces[i].v_id[0]);						edges.push_back(edge1);
		Edge edge2(i, faces[i].v_id[0], faces[i].v_id[1]);						edges.push_back(edge2);

		vertices[faces[i].v_id[1]].ConnectedVertices.push_back(faces[i].v_id[2]);
		vertices[faces[i].v_id[2]].ConnectedVertices.push_back(faces[i].v_id[0]);
		vertices[faces[i].v_id[0]].ConnectedVertices.push_back(faces[i].v_id[1]);
	}
	edges_size = edges.size();

	for(int i = 0; i < edges_size-1; ++i){
		for(int j = i+1; j < edges_size; ++j){
			if((edges[i].start == edges[j].end) && (edges[i].end == edges[j].start)){
				edges[i].counterpart_id = j;
				edges[j].counterpart_id = i;
				break;
			}
		}
	}

	// association of Vertex with Edge, Vertex and Edge with Face
	for(int i = 0; i < edges_size; ++i){
		Vertex *v0 = &vertices[edges[i].start];
		Vertex *v1 = &vertices[edges[i].end];
		edges[i].setVertex(v0, v1);
	}

	for(int i = 0; i < faces_size; ++i){
		Vertex *v0 = &vertices[faces[i].v_id[0]];
		Vertex *v1 = &vertices[faces[i].v_id[1]];
		Vertex *v2 = &vertices[faces[i].v_id[2]];
		faces[i].setVertex(v0, v1, v2);

		Edge *e0 = &edges[i*3];
		Edge *e1 = &edges[i*3+1];
		Edge *e2 = &edges[i*3+2];
		faces[i].setEdge(e0, e1, e2);
	}

	// set the stress-free area and stress-free length
	for(int i = 0; i < faces_size; ++i){
		double area = faces[i].calcNormal().norm();
		area /= 2.0;
		faces[i].natural_area = area;
	}

	for(int i = 0; i < edges_size; ++i){
		_vec<double> distance = edges[i].edgeVector();
		double dist = distance.norm();
		edges[i].setNaturalLength(dist);
	}

	for(int i = 0; i < fixpolygon.FixVertices.size(); ++i){
		_vec<double> Vf = fixpolygon.FixVertices[i].loc;
		for(int j = 0; j < vertices_size; ++j){
			_vec<double> Vm = vertices[j].loc;
			_vec<double> diff = Vf - Vm;
			if(diff.norm() < 1e-2){
//		       	if((Vf.x == Vm.x)&&(Vf.y == Vm.y)&&(Vf.z == Vm.z)){
			  fixpolygon.FixVertexID.push_back(j);
			  break;
			}
		}
	}
	
	for(int i = 0; i < fixpolygon.FixVertexID.size(); ++i){
		vertices[fixpolygon.FixVertexID[i]].fix = 1;
	}

	{
		//Calculate the angle
		int f_id_0, f_id_1;
		for(int i = 0; i < edges_size; ++i){
			if(edges[i].counterpart_id == -1){
				_vec<double> v0 = _vec<double> (1.0, 0.0, 0.0);
				edges[i].calcAngle(v0, v0);
				edges[i].setNaturalAngle();
			}else{
				f_id_0 = edges[i].belongFace_id;
				f_id_1 = edges[edges[i].counterpart_id].belongFace_id;

				edges[i].calcAngle(faces[f_id_0].calcNormal(), faces[f_id_1].calcNormal());
				edges[i].setNaturalAngle();
			}
		}
	}

#ifdef COLLISION
	// set the collision distance
	collision_distance = 0.0;
	for(int i = 0; i < edges_size; ++i){
		_vec<double> distance = vertices[edges[i].end].loc - vertices[edges[i].start].loc;
		double dist = distance.norm();
		collision_distance += dist;
	}
	collision_distance /= edges_size;
	collision_distance *= 2.;
//	collision_distance *= 1.2;

	cout << "collision distance = " << collision_distance << endl;
#endif

	// set color
	set_color();
}

//output
void outputVTK(int step){
	ostringstream oss_step;
	oss_step.setf(ios::right);
	oss_step.fill('0');
	oss_step.width(8);
	oss_step << step;
	string output_filename = "./output_" + org_name + "/um" + oss_step.str() + ".vtk";

	ofstream ofs(output_filename);
	if(!ofs){	cout << "fail to output the file!" << endl;	exit(1);}

	ofs << "# vtk DataFile Version 2.0" << endl;
	ofs << "Gaussian Curvature" << endl;
	ofs << "ASCII" << endl;
	ofs << "DATASET UNSTRUCTURED_GRID" << endl;

	ofs << "POINTS " << vertices_size << " float" << endl;
	for(auto vp = vertices.begin(); vp != vertices.end(); ++vp){	ofs << vp->loc.x << " " << vp->loc.y << " " << vp->loc.z << endl;}

	ofs << "CELLS " << faces_size << " " << 4*faces_size << endl;
	for(auto fp = faces.begin(); fp != faces.end(); ++fp){			ofs << "3 " << fp->v_id[0] << " " << fp->v_id[1] << " " << fp->v_id[2] << endl;}

	ofs << "CELL_TYPES " << faces_size << endl;
	for(auto fp = faces.begin(); fp != faces.end(); ++fp){			ofs << "5" << endl;}

	ofs.close();
}

void outputParam(){
	string ofname = "./output_" + org_name + "/parameter.txt";
	ofstream ofs(ofname);
	if(!ofs){	cout << "fail to output the file!" << endl;	exit(1);}
	ofs << org_name << endl;
	ofs << fix_name << endl;
	ofs << "spring_k_e: " << spring_k_e << endl;
	ofs << "spring_k_f: " << spring_k_f << endl;
	ofs << "k_n       : " << k_n << endl;
	ofs << "spring_k_a: " << spring_k_a << endl;
#ifdef COLLISION
	ofs << "ene_repulsive: " << ene_repulsive << endl;
#endif
	ofs.close();
}


#ifdef COLLISION
void normal_vector_update(){
	for(int i = 0; i < vertices.size(); ++i){	vertices[i].vertex_normal = _vec<double>(0.0, 0.0, 0.0);}

	for(int i = 0; i < faces_size; ++i){
		_vec<double> normal = faces[i].calcNormal();
		for(int j = 0; j < 3; ++j){
			vertices[faces[i].v_id[j]].vertex_normal += normal;
		}
	}
}

//void direct_solver(){
//	int flag;
//
//	for(int i = 0; i < vertices_size-1; ++i){
//		for(int j = i+1; j < vertices_size; ++j){
//			flag = 1;
//
//			for(int k = 0; k < vertices[i].ConnectedVertices.size(); ++k){
//				if(j == vertices[i].ConnectedVertices[k])	flag = 0;
//			}
//
//			if(flag == 0){ // do nothing
//			}else if(vertices[i].fix != 1 || vertices[j].fix != 1){
//				_vec<double> dij = vertices[j].loc - vertices[i].loc;
//				double dist = dij.norm();
//
//				if(dist < collision_distance){
//					_vec<double> normal_i = vertices[i].vertex_normal;
//					_vec<double> normal_j = vertices[j].vertex_normal;
//					double mag_i = normal_i.norm();	if(mag_i < 1e-8){ mag_i += 1e-8;}	normal_i /= mag_i;
//					double mag_j = normal_j.norm();	if(mag_j < 1e-8){ mag_j += 1e-8;}	normal_j /= mag_j;
//
//					_vec<double> mean_vec = normal_i - normal_j;
//					mean_vec *= 0.50;	mean_vec /= collision_distance;
//
//					_vec<double> forceCollision = -ene_repulsive * (1.0 - (dij*mean_vec)) * mean_vec;
//
//					vertices[i].Force(forceCollision);
//					forceCollision *= -1.0;
//					vertices[j].Force(forceCollision);
//				}
//			}
//		}
//	}
//}




void calc_repulsive_force_ij(Vertex *pi, Vertex *pj){
	int flag = 0;
	for(auto vi: pi->ConnectedVertices){
		if(vi == pj->vi) flag = 1;
	}

	_vec<double> dij = pj->loc - pi->loc;
	double dist = dij.norm();

	if(dist < collision_distance && flag == 0){
		_vec<double> normal_i = pi->vertex_normal;
		_vec<double> normal_j = pj->vertex_normal;
		normal_i /= normal_i.norm();
		normal_j /= normal_j.norm();

//		_vec<double> mean_vec = normal_i - normal_j;
//		mean_vec *= 0.50;	mean_vec /= collision_distance;
//
//		_vec<double> forceCollision = -ene_repulsive * (1.0-(dij*mean_vec)) * mean_vec;

#ifdef NO_TANGENT
		// tangential force is neglected
		_vec<double> mean_vec1 = normal_i - normal_j;
		_vec<double> mean_vec2 = normal_i + normal_j;

		_vec<double> mean_vec = mean_vec1;
		if(mean_vec1.norm() < mean_vec2.norm()) mean_vec = mean_vec2;

		mean_vec /= mean_vec.norm();
#endif

		_vec<double> unit_vec = dij;
		unit_vec /= dist;

#ifdef NO_TANGENT
		//dist = mean_vec*dij; dist = sqrt(dist*dist);
		unit_vec = (unit_vec*mean_vec)*mean_vec;
#endif
		_vec<double> forceCollision = ene_repulsive * (dist/collision_distance - 1.0)/collision_distance*unit_vec;

		//pi->Force(forceCollision);
		pi->Force_omp(forceCollision);
		forceCollision *= -1.0;
		//pj->Force(forceCollision);
		pj->Force_omp(forceCollision);

//		pi->Force(forceCollision);
//		forceCollision *= -1.0;
//		pj->Force(forceCollision);
	}
}


void same_node_micro_list_seeking(int j, Vertex *p_i){
	if(!node[j].i.empty()){
		vector<int>::iterator vi = node[j].i.begin();
		while(vi != node[j].i.end()){
			Vertex* p_j = &vertices[*vi];
	
			if(p_i->vi < p_j->vi){
				(void)calc_repulsive_force_ij(p_i, p_j);
			}
			++vi;
		}
	}
}



void adjacent_node_micro_list_seeking(int j, Vertex *p_i){
	if(!node[j].i.empty()){
		vector<int>::iterator vi = node[j].i.begin();
	
		while(vi != node[j].i.end()){
			Vertex *p_j =&vertices[*vi];
	
			{
				(void)calc_repulsive_force_ij(p_i, p_j);
			}
			++vi;
		}
	}
}


/*
void micro_list_seeking(int j, Vertex *p_i){
	vector<Vertex*>::iterator p_j = node[j].pp.begin();
	while(p_j != node[j].pp.end()){
		if(p_i ->fix != 1 || (*p_j) ->fix != 1){
			if(p_i->vi < (*p_j)->vi){	(void)calc_repulsive_force_ij(p_i, (*p_j));}
		}
		++p_j;
	}
}
*/



void list_solver(){
	//for(int i = 0; i < (SYS_SIZE*SYS_SIZE*SYS_SIZE); ++i){
	//openmpにするとなぜか遅い by inoue 2019.10.02
	//memory bandのよいPCを使うべきか。
#pragma omp parallel for num_threads(THREAD_NUM)
	for( int k = 0; k < vertices_size; k++){
		Vertex* p_i = &vertices[k];
		_vec<double> loc = p_i->loc + SHIFT;
		loc /= _list_dsize;
		_vec<int> iloc = loc.icast();
		int x = iloc.x;
		int y = iloc.y;
		int z = iloc.z;
		if(iloc.x < 0 || iloc.y < 0 || iloc.z < 0 || iloc.x >= SYS_SIZE || iloc.y >= SYS_SIZE || iloc.z >= SYS_SIZE){
			std::cout << "Error - Out of System size" << std::endl; exit(1);
		}
//		int x = i / (SYS_SIZE*SYS_SIZE);
//		int y = (i % (SYS_SIZE*SYS_SIZE)) / SYS_SIZE;
//		int z = (i % (SYS_SIZE*SYS_SIZE)) % SYS_SIZE;

		int i = x * SYS_SIZE * SYS_SIZE + y * SYS_SIZE + z;
		int xp = x+1;
		if(xp >= SYS_SIZE) 	xp -= SYS_SIZE;
		int xn = x-1;
		if(xn < 0)			xn += SYS_SIZE;

		int yp = y+1;
		if(yp >= SYS_SIZE)	yp -= SYS_SIZE;
		int yn = y-1;
		if(yn < 0)			yn += SYS_SIZE;

		int zp = z+1;
		if(zp >= SYS_SIZE)	zp -= SYS_SIZE;
		int zn = z-1;
		if(zn < 0)			zn += SYS_SIZE;

		//vector<Vertex*>::iterator p_i = node[i].pp.begin();
		int count = 0;

/*
		nb_list[imap] = getGridId(gx - 1, gy - 1, gz - 1);
		nb_list[imap + 1] = getGridId(gx, gy - 1, gz - 1);
		nb_list[imap + 2] = getGridId(gx + 1, gy - 1, gz - 1);

		nb_list[imap + 3] = getGridId(gx - 1, gy, gz - 1);
		nb_list[imap + 4] = getGridId(gx, gy, gz - 1);
		nb_list[imap + 5] = getGridId(gx + 1, gy, gz - 1);

		nb_list[imap + 6] = getGridId(gx - 1, gy + 1, gz - 1);
		nb_list[imap + 7] = getGridId(gx, gy + 1, gz - 1);
		nb_list[imap + 8] = getGridId(gx + 1, gy + 1, gz - 1);

		nb_list[imap + 9] = getGridId(gx - 1, gy - 1, gz);
		nb_list[imap + 10] = getGridId(gx, gy - 1, gz);
		nb_list[imap + 11] = getGridId(gx + 1, gy - 1, gz);

		nb_list[imap + 12] = getGridId(gx - 1, gy, gz);
*/

		//while(p_i != node[i].pp.end())
		{
			//3x3x3 = 27 nodes:: 13 + 1 of 27 nodes should be calculate. Using symmetry, the rest of nodes are refered in another tern.
            /**/
			// {	int j = xp * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + z;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp
			// {	int j = x  * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + z;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//yp
			// {	int j = x  * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//zp
			// {	int j = xp * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + z;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp yp
			// {	int j = xn * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + z;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn yp
			// {	int j = xp * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp zp
			// {	int j = xn * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn zp
			// {	int j = x  * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//yp zp
			// {	int j = x  * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//yn zp
			// {	int j = xp * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp yp zp
			// {	int j = xn * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn yp zp
			// {	int j = xn * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn yn zp
			// //inoue revised 12.31.2019
			// {	int j = xp * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zp;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp yn zp
            /**/
            
            {	int j = i;												(void)same_node_micro_list_seeking(j, p_i);	}	//self
            {	int j = xn * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + z;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn
			{	int j = x  * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + z; 	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//yn
			{	int j = x  * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//zn
			{	int j = xp * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + z;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp yn
			{	int j = xn * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + z;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn yn
			{	int j = xp * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp zn
			{	int j = xn * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn zn
			{	int j = x  * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//yp zn
			{	int j = x  * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//yn zn
			{	int j = xp * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp yp zn
			{	int j = xp * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xp yn zn
			{	int j = xn * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn yp zn			
			{	int j = xn * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zn;	(void)adjacent_node_micro_list_seeking(j, p_i);	}	//xn yn zn



		//++p_i;
		}
	}
}


/*
void list_solver(){






	for(int i = 0; i < (SYS_SIZE*SYS_SIZE*SYS_SIZE); ++i){
		int x = i / (SYS_SIZE*SYS_SIZE);
		int y = (i % (SYS_SIZE*SYS_SIZE)) / SYS_SIZE;
		int z = (i % (SYS_SIZE*SYS_SIZE)) % SYS_SIZE;

		int xp = x+1;
		if(xp >= SYS_SIZE) 	xp -= SYS_SIZE;
		int xn = x-1;
		if(xn < 0)			xn += SYS_SIZE;

		int yp = y+1;
		if(yp >= SYS_SIZE)	yp -= SYS_SIZE;
		int yn = y-1;
		if(yn < 0)			yn += SYS_SIZE;

		int zp = z+1;
		if(zp >= SYS_SIZE)	zp -= SYS_SIZE;
		int zn = z-1;
		if(zn < 0)			zn += SYS_SIZE;

		vector<Vertex*>::iterator p_i = node[i].pp.begin();
		int count = 0;
		while(p_i != node[i].pp.end()){
			{	int j = i;												(void)micro_list_seeking(j, (*p_i));	}	//self
			{	int j = xp * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//xp
			{	int j = xn * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//xn
			{	int j = x  * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//yp
			{	int j = x  * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//yn
			{	int j = x  * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//zp
			{	int j = x  * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//zn
			{	int j = xp * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//xp yp
			{	int j = xp * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//xp yn
			{	int j = xn * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//xn yp
			{	int j = xn * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + z;	(void)micro_list_seeking(j, (*p_i));	}	//xn yn
			{	int j = xp * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//xp zp
			{	int j = xp * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//xp zn
			{	int j = xn * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//xn zp
			{	int j = xn * SYS_SIZE * SYS_SIZE + y  * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//xn zn
			{	int j = x  * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//yp zp
			{	int j = x  * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//yp zn
			{	int j = x  * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//yn zp
			{	int j = x  * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//yn zn
			{	int j = xp * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//xp yp zp
			{	int j = xp * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//xp yp zn
			{	int j = xp * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//xp yn zn
			{	int j = xn * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//xn yp zp
			{	int j = xn * SYS_SIZE * SYS_SIZE + yp * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//xn yp zn
			{	int j = xn * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zp;	(void)micro_list_seeking(j, (*p_i));	}	//xn yn zp
			{	int j = xn * SYS_SIZE * SYS_SIZE + yn * SYS_SIZE + zn;	(void)micro_list_seeking(j, (*p_i));	}	//xn yn zn

		++p_i;
		}
	}
}
*/


void collision(){
	(void)normal_vector_update();


	// clock_t pre_start = clock();
	// for(auto np = node.begin(); np != node.end(); ++np){
	// 	// if(!np->pp.empty())	np->pp.clear();
	// 	if(!np->i.empty() ) np->i.clear();
	// }
	
	int chk = 0;
	// for(auto v: vertices){
	// 	_vec<double> loc = v.loc + SHIFT;
	// 	loc /= _list_dsize;
	// 	_vec<int> iloc = loc.icast();
	// 	if(iloc.x < 0 || iloc.y < 0 || iloc.z < 0 || iloc.x >= SYS_SIZE || iloc.y >= SYS_SIZE || iloc.z >= SYS_SIZE){
	// 		chk = 1;
	// 		cout << SYS_SIZE << endl;
	// 		break;
	// 	}else{
	// 		int index = iloc.x*SYS_SIZE*SYS_SIZE + iloc.y*SYS_SIZE + iloc.z;
	// 		// node[index].pp.push_back(&v);
	// 		node[index].i.push_back(v.vi);
	// 	}
	// }


	for( int i = 0; i < vertices.size(); ++i){
		Vertex* v = &vertices[i];
		_vec<double> loc = v->loc + SHIFT;
		loc /= _list_dsize;
		_vec<int> iloc = loc.icast();
		if(iloc.x < 0 || iloc.y < 0 || iloc.z < 0 || iloc.x >= SYS_SIZE || iloc.y >= SYS_SIZE || iloc.z >= SYS_SIZE){
			chk = 1;
			cout << SYS_SIZE << endl;
			break;
		}else{
			int index = iloc.x*SYS_SIZE*SYS_SIZE + iloc.y*SYS_SIZE + iloc.z;
			// node[index].pp.push_back(&v);
			int j = v->pull_nodeid();
			if( index != j ){
				std::vector<int>::iterator it = std::find(node[j].i.begin(), node[j].i.end(), v->vi);

				// std::cout << *(node[j].i.end() - 1) << " " << *node[j].i.end() << std::endl;


				std::iter_swap(it,(node[j].i.end()-1));// end()は末尾の次の要素を指すので、-1して末尾要素を参照する。末尾と交換
				node[j].i.pop_back();//末尾要素を削除

				v->push_nodeid(index);//粒子側のnode indexの更新
				node[index].i.push_back(v->vi);//node に粒子idを登録
			}
		}
	}
	

	// clock_t pre_end = clock();

	// const double pre_time = static_cast<double>(pre_end - pre_start) / CLOCKS_PER_SEC;
	// printf("pretime %f\n", pre_time);

	if(chk == 1){
		//std::cout << "Direct Solver" << std::endl;
		//(void)direct_solver();
		std::cout << "Expand box size" << std::endl;

		_vec<double> min = vertices[0].loc;
		_vec<double> max = vertices[0].loc;

		for(auto v: vertices){
			if(v.loc.x < min.x) min.x = v.loc.x;
			if(v.loc.x > max.x) max.x = v.loc.x;

			if(v.loc.y < min.y) min.y = v.loc.y;
			if(v.loc.y > max.y) max.y = v.loc.y;

			if(v.loc.z < min.z) min.z = v.loc.z;
			if(v.loc.z > max.z) max.z = v.loc.z;
		}

		//inoue revised 2019.12.27
		const double tmp_margin_n = 2.0;
		
		
		
		_vec<double> box_size = max - min;	box_size *= tmp_margin_n;
		SHIFT = -1.0 * min + _list_dsize * 0.50 + box_size * (tmp_margin_n - 1.) * 0.50 / tmp_margin_n;

		//_vec<double> box_size = max-min; box_size *= 5.0;
		//SHIFT = -1.0*min + _list_dsize*0.50 + box_size*0.40;

		box_size /= _list_dsize;
		_vec<int> tmp_sys_size = box_size.icast();

		//int tmp_SYS_SIZE = SYS_SIZE;

		SYS_SIZE = tmp_sys_size.x;

		if(SYS_SIZE < tmp_sys_size.y)	SYS_SIZE = tmp_sys_size.y;
		if(SYS_SIZE < tmp_sys_size.z)	SYS_SIZE = tmp_sys_size.z;

		//int diff = SYS_SIZE*SYS_SIZE*SYS_SIZE - tmp_SYS_SIZE*tmp_SYS_SIZE*tmp_SYS_SIZE;
		//std::cout << "box increased by" << diff << std::endl;
		//
		//_list tmp;
		//for(int i = 0; i < diff; ++i)	node.push_back(tmp);

//		cout << "Direct Solver" << endl;
		if(SYS_SIZE < 0){
			cout << "Negative SYS_SIZE error" << endl;	exit(1);
		}
		
		std::cout << "resizing" << std::endl;
		_list ptcl_tmp;
		node.resize(SYS_SIZE * SYS_SIZE * SYS_SIZE, ptcl_tmp);
		std::cout << "resize succeeded" << std::endl;
		std::cout << SYS_SIZE << " " << node.size() << std::endl;

		
		for(auto np = node.begin(); np != node.end(); ++np){
			// if(!np->pp.empty())	np->pp.clear();
			if(!np->i.empty()) 	np->i.clear();
		}
		

		chk = 0;

		for(int i = 0; i < vertices.size(); ++i){
			Vertex* v = &vertices[i];
			_vec<double> loc = v->loc + SHIFT;
			loc /= _list_dsize;
			_vec<int> iloc = loc.icast();
			if(iloc.x < 0 || iloc.y < 0 || iloc.z < 0 || iloc.x >= SYS_SIZE || iloc.y >= SYS_SIZE || iloc.z >= SYS_SIZE){
				chk = 1;
				cout << SYS_SIZE << endl;
				std::cout << v->vi << " " << iloc.x << " " << iloc.y << " " << iloc.z << std::endl;
				cout << "Error -expand";
				exit(1);
			}else{
				int index = iloc.x*SYS_SIZE*SYS_SIZE + iloc.y*SYS_SIZE + iloc.z;
				std::cout << "pushback" << std::endl;
				// node[index].pp.push_back(&v);
				node[index].i.push_back(v->vi);
				v->push_nodeid(index);
				std::cout << "pushback succeeded" << std::endl;
			}
		}
	}
	
	if(chk == 0){
		// clock_t start = clock();

		//std::cout << "List Solver Start" << std::endl;
		(void)list_solver();
		//std::cout << "List Solver End" << std::endl;


		// clock_t end = clock();

		// const double time = static_cast<double>(end - start) / CLOCKS_PER_SEC;
		// printf("coll::time %f\n", time);
	}else{
		std::cout << "Error - beyond box size " << std::endl;
		exit(1);
	}
}
#endif

/*
void collision(){
	(void)normal_vector_update();


	for(auto np = node.begin(); np != node.end(); ++np){
		if(!np->pp.empty())	np->pp.clear();
	}

	int chk = 0;
	for(auto v: vertices){
		_vec<double> loc = v.loc + SHIFT;
		loc /= _list_dsize;
		_vec<int> iloc = loc.icast();
		if(iloc.x < 0 || iloc.y < 0 || iloc.z < 0 || iloc.x >= SYS_SIZE || iloc.y >= SYS_SIZE || iloc.z >= SYS_SIZE){
			chk = 1;
			cout << SYS_SIZE << endl;
			break;
		}else{
			int index = iloc.x*SYS_SIZE*SYS_SIZE + iloc.y*SYS_SIZE + iloc.z;
			node[index].pp.push_back(&v);
		}
	}

	if(chk == 1){
		(void)direct_solver();

		_vec<double> min(-1000.0, -1000.0, -1000.0);
		_vec<double> max(1000.0, 1000.0, 1000.0);

		for(auto v: vertices){
			if(v.loc.x < min.x) min.x = v.loc.x;
			if(v.loc.x > max.x) max.x = v.loc.x;

			if(v.loc.y < min.y) min.y = v.loc.y;
			if(v.loc.y > max.y) max.y = v.loc.y;

			if(v.loc.z < min.z) min.z = v.loc.z;
			if(v.loc.z > max.z) max.z = v.loc.z;
		}

		_vec<double> box_size = max-min; box_size *= 5.0;
		SHIFT = -1.0*min + _list_dsize*0.50 + box_size*0.40;

		box_size /= _list_dsize;
		_vec<int> tmp_sys_size = box_size.icast();

		int tmp_SYS_SIZE = SYS_SIZE;

		SYS_SIZE = tmp_sys_size.x;

		if(SYS_SIZE < tmp_sys_size.y)	SYS_SIZE = tmp_sys_size.y;
		if(SYS_SIZE < tmp_sys_size.z)	SYS_SIZE = tmp_sys_size.z;

		int diff = SYS_SIZE*SYS_SIZE*SYS_SIZE - tmp_SYS_SIZE*tmp_SYS_SIZE*tmp_SYS_SIZE;
		_list tmp;

		for(int i = 0; i < diff; ++i)	node.push_back(tmp);

		cout << "Direct Solver" << endl;
		if(SYS_SIZE < 0){
			cout << "Negative SYS_SIZE error" << endl;	exit(1);
		}
	}else{
		(void)list_solver();
	}
}
#endif

*/

void Force_calc(double phys_time){

#pragma omp parallel num_threads(THREAD_NUM)
	{
		//force from edge
#pragma omp for
		for (int i = 0; i < edges_size; ++i) {
			edges[i].forceEdge();
		}

		// calculate the inter-facet angle
		//int f_id_0, f_id_1;
#pragma omp for
		for (int i = 0; i < edges_size; ++i) {

			if (edges[i].counterpart_id == -1) {
				_vec<double> v0 = _vec<double>(1.0, 0.0, 0.0);
				edges[i].calcAngle(v0, v0);
			}
			else {
				int f_id_0 = edges[i].belongFace_id;//moved this declaration here to be a private variable
				int f_id_1 = edges[edges[i].counterpart_id].belongFace_id;//moved this declaration here to be a private variable

				edges[i].calcAngle(faces[f_id_0].calcNormal(), faces[f_id_1].calcNormal());
			}
		}

		// force from volume, face, angle
#pragma omp for
		for (int i = 0; i < faces_size; ++i) {
			faces[i].forceFace();
			faces[i].forceNormal(phys_time);
			faces[i].forceAngle();
		}
		// reduce force_omp to force
	}
#ifdef COLLISION
	(void)collision();
#endif

#pragma omp parallel for num_threads(THREAD_NUM)
		for (int i = 0; i < vertices_size; ++i) {
			vertices[i].reduceForce_omp();
		}
	


/*	
	// the force of fixed vertices into 0
	for(int i = 0; i < fixpolygon.FixVertexID.size(); ++i){
		vertices[fixpolygon.FixVertexID[i]].ForceToZero();
	}
*/

}


/* SUNDIALS CVODE declarations ----------------------- */
static int ODEs(realtype t, N_Vector y, N_Vector ydot, void *user_data)
{
  //realtype *ydata  = N_VGetArrayPointer(y);
  //realtype *dydata = N_VGetArrayPointer(ydot);
	realtype *ydata = NV_DATA_OMP(y);
	realtype *dydata = NV_DATA_OMP(ydot);

  // copy variables values from CVODE to vertices
  // need to do this because 'total energy function' is computed from vertices
#pragma omp parallel for num_threads(THREAD_NUM)
  for (int i = 0; i < vertices.size(); ++i) {
    vertices[i].loc.x = ydata[i*3 + 0]; 
    vertices[i].loc.y = ydata[i*3 + 1]; 
    vertices[i].loc.z = ydata[i*3 + 2]; 
    //vertices[i].ForceToZero();//redundant calculation with ~.reduceForce_omp()
  }

	Force_calc(t);
#pragma omp parallel for num_threads(THREAD_NUM)
  for (int i = 0; i < vertices.size(); ++i) {
    if (vertices[i].fix != 1) {
      _vec<double> k1 = vertices[i].call_frc();
      dydata[i*3 + 0] = k1.x;
      dydata[i*3 + 1] = k1.y;
      dydata[i*3 + 2] = k1.z;
    }
    else {
      dydata[i*3 + 0] = 0.0;
      dydata[i*3 + 1] = 0.0;
      dydata[i*3 + 2] = 0.0;
    }
  }

  return 0;
}


realtype cvode_reltol, cvode_abstol, cvode_time;
N_Vector cvode_y = NULL;
void *cvode_mem = NULL;
SUNMatrix cvode_A = NULL;
SUNLinearSolver cvode_LS = NULL;

const realtype T0 = 0.0; // initial time
const realtype DT = 0.0001; // time step
const realtype output_time = 0.010;
const realtype FT = 15.0; // final simulation time

int NEQ = 0; // number of equations, vertices * 3

//#define IJKth(vdata,i,j,k) vdata[(i)*GRID_COLS*NV + (j)*NV + k]

/* --------------------------------------------------- */
static void atExit (void) {

  /* Free y and abstol vectors */
  if (cvode_y != NULL){
    N_VDestroy(cvode_y);
    cvode_y = NULL;
  }
  
  /* Free integrator memory */
  if (cvode_mem != NULL){
    CVodeFree(&cvode_mem);
    cvode_mem = NULL;
  }
  
  /* Free linear solver memory  */
  if (cvode_LS != NULL){
    SUNLinSolFree(cvode_LS); 
    cvode_LS = NULL;
  }
      
  /* Free the matrix memory     */
  if (cvode_A != NULL){
    SUNMatDestroy(cvode_A); 
    cvode_A = NULL;
  }
  return;
}



int main(){
  atexit(atExit);

  edges.clear();
  vertices.clear();
  faces.clear();

  init();
#ifdef COLLISION
  Init_Collision_list();
#endif
  outputParam();

  outputVTK(0);

  NEQ = vertices.size() * 3; // the number of equations

  // SUNDIALS CVODE initialization
  // create serial vector for initial conditions
  //if ((cvode_y = N_VNew_Serial(NEQ)) == NULL){
  if ((cvode_y = N_VNew_OpenMP(NEQ, THREAD_NUM)) == NULL) {
  	cerr << "Error - cannot allocate memory for state vector." << endl;
    atExit();
    exit(1);
  }

  // set initial conditions and tolerances
 // realtype *cvode_ydata = N_VGetArrayPointer(cvode_y);
 /*
  realtype *cvode_ydata = NV_DATA_OMP(cvode_y);
  for (int i = 0; i < vertices.size(); ++i) {
    cvode_ydata[i*3 + 0] = vertices[i].loc.x;
    cvode_ydata[i*3 + 1] = vertices[i].loc.y;
    cvode_ydata[i*3 + 2] = vertices[i].loc.z;
  }
  cvode_time = 0.0;
  cvode_reltol = RCONST(1.0e-6);
  cvode_abstol = RCONST(1.0e-5);
  */
  /* Call CVodeCreate to create the solver memory and specify the 
   * Backward Differentiation Formula and the use of a Newton iteration */
  /*
  if ((cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON)) == NULL){
  	cerr << "Error - cannot allocate memory for CVODE solver." << endl;
    atExit();
    exit(1);
  }
	*/
	/*
  if ((cvode_y = N_VNew_OpenMP(NEQ, THREAD_NUM)) == NULL) {
  	cerr << "Error - cannot allocate memory for state vector." << endl;
    atExit();
    exit(1);
  }
  */
    // set initial conditions and tolerances
 // realtype *cvode_ydata = N_VGetArrayPointer(cvode_y);
  realtype *cvode_ydata = NV_DATA_OMP(cvode_y);
  for (int i = 0; i < vertices.size(); ++i) {
    cvode_ydata[i*3 + 0] = vertices[i].loc.x;
    cvode_ydata[i*3 + 1] = vertices[i].loc.y;
    cvode_ydata[i*3 + 2] = vertices[i].loc.z;
  }
  cvode_time = 0.0;
  cvode_reltol = RCONST(1.0e-4);//modified from 1.0e-6
  cvode_abstol = RCONST(1.0e-5);//modified from 1.0e-5
  
  /* Call CVodeCreate to create the solver memory and specify the 
   * Backward Differentiation Formula and the use of a Newton iteration */
//  if ((cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON)) == NULL){
	if ((cvode_mem = CVodeCreate(CV_BDF)) == NULL){
  	cerr << "Error - cannot allocate memory for CVODE solver." << endl;
    atExit();
    exit(1);
  }
  
  /* Call CVodeInit to initialize the integrator memory and specify the
   * user's right hand side function in y'=f(t,y), the inital time T0, and
   * the initial dependent variable vector y. */
  if (CVodeInit(cvode_mem, ODEs, T0, cvode_y) != CV_SUCCESS){
  	cerr << "Error - cannot initialize CVODE solver." << endl;
    atExit();
    exit(1);
  }

  /* Call CVodeSStolerances to specify the scalar relative tolerance
   * and vector absolute tolerances */
  if (CVodeSStolerances(cvode_mem, cvode_reltol, cvode_abstol)){
  	cerr << "Error - cannot set tolerances for CVODE solver." << endl;
    atExit();
    exit(1);
  }

  /* Create banded SUNLinearSolver object for use by CVode */
  if ((cvode_LS = SUNSPGMR(cvode_y,PREC_NONE,0)) == NULL){
  	cerr << "Error - cannot set SUNLinearSovler." << endl;
    atExit();
    exit(1);
  }
  
  if (CVSpilsSetLinearSolver(cvode_mem, cvode_LS)){
  	cerr << "Error - cannot attach matrix and linear solver to CVode." << endl;
    atExit();
    exit(1);
  }
  while (cvode_time < FT) {
    //cout << phys_time << endl;

    if (CVode(cvode_mem, cvode_time+DT, cvode_y, &cvode_time, CV_NORMAL) != CV_SUCCESS) {
    	cerr << "Error - rewind simulation or recompile the model." << endl;
    	atExit();
    	exit(1);
    }

    unsigned long int step = (unsigned long int)(cvode_time/DT);
    unsigned long int step_out = (unsigned long int)(output_time/DT);

    if((step%step_out) == 0) {
      outputVTK((int)(step/step_out));
      cout << "(output) physical time = " << cvode_time << endl;
    }
  }
	cerr << "Simulation finished." << endl;
	atExit();
	exit(0);
}
