#include "vec.h"
#include "parameter.h"
#include "Vertex.h"
#include "Edge.h"


Edge::Edge(int Face_id, int _i,int _j){
	start = _i;
	end = _j;
	belongFace_id = Face_id;
	counterpart_id = -1;
}

void Edge::setVertex(Vertex *Start, Vertex *End){
	start_address = Start;
	end_address = End;
}

void Edge::setNaturalAngle(){
	//natural_angle = theta;
	natural_angle = 0.;
}

void Edge::setNaturalLength(double _l){
	natural_length = _l;
}

void Edge::forceEdge(){
	_vec<double> distance = edgeVector();
	double dist = distance.norm();
	
	double norm = dist;
	if(dist < 1e-8)	dist += 1e-8;
	distance /= norm;

	double force = -spring_k_e*(dist - natural_length);

	//end_address->Force(force * distance);
	end_address->Force_omp(force * distance);

	if(counterpart_id == -1){
		force *= -1;
		//start_address->Force(force * distance);
		start_address->Force_omp(force * distance);
	}
}

void Edge::calcAngle(_vec<double> v0, _vec<double> v1){
	_vec<double> cross_product = v0 % v1;
	_vec<double> edge = edgeVector();
	edge /= edge.norm();
	double cross = cross_product * edge;
	double dot = v0 * v1;
	theta = atan2(cross, dot);
}

_vec<double> Edge::edgeVector(){
	return end_address->loc - start_address->loc;
}

double Edge::angleDisplacement(){
	return theta - natural_angle;
}