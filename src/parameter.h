#ifndef parameter_h
#define parameter_h

#include <string>

/* collision macro: if you want to consider collision, define COLLISION*/
#define COLLISION

/* using filenames (only obj file can be loaded and without writing .obj) */
const std::string org_name 	= "horn_primordia_11332_rep10_base";				// the name of mesh you want to extend
const std::string fix_name 	= "horn_imaginal_disc_11332_test_fix";					// the name of mesh you want to fix

/* parameters for energy minimization */
const double spring_k_e = 1e+5;//1e+5;			// edge length elasticity
const double spring_k_f = 3e+1;//30.0;			// facet area elasticity
const double spring_k_a = 1e+0;//1.0;			// inter-facets angle elasticity
const double k_n = 3e+1;				// coefficient of vertical force to each facet
const double ene_repulsive = 2e+5;//1e-1;//1e+5;		// coefficient of collision energy

/* OpenMP */
constexpr int THREAD_NUM = 16;
//constexpr int num_threads=8;//The number of threads in OpenMP parallel computations

#endif
